<?php

namespace App\Http\Controllers\Api;

use App\models\Incident;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IncidentController extends Controller
{

    private $incident;

    public function __construct(Incident $incident)
    {
        $this->incident = $incident;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $incident = $this->incident->all();
        return response()->json([
            'data' => $incident
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        if($data['status'] == '') {
            $data['status'] = 'Aberto';
        }
        try{
            $incident = $this->incident->create($data);
            return response()->json([
                'data' => [
                    'msg' => 'Incidente cadastrado com sucesso!'
                ]
            ], 200);
        }catch(\Exception $e) {
            return response()->json($e->getMessage(), 401);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $incident = $this->incident->findOrFail($id);
            return response()->json([
                'data' => $incident
            ], 200);
        }catch(\Exception $e) {
            return response()->json($e->getMessage(), 401);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        try{
            $incident = $this->incident->findOrFail($id);
            $incident->update($data);
            return response()->json([
                'data' => [
                    'msg' => 'Dados atualizados com sucesso'
                ]
            ]);
        }catch(\Exception $e) {
            return response()->json($e->getMessage(), 401);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $incident = $this->incident->findOrFail($id);
            $incident->delete();
            return response()->json([
                'data' => [
                    'msg' => 'Incidente deletado com sucesso.'
                ]
            ], 200);
        }catch(\Exception $e) {
            return response()->json($e->getMessage(), 401);
        }
    }
}
