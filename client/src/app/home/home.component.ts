import { Component, OnInit, ViewChild} from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Incident } from '../models/incident.model'
import { HelperService } from '../services/helper.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  displayedColumns: string[] = ['title', 'description', 'criticality', 'type', 'status', 'opcoes'];
  dataSource:any = [];

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  constructor(
    private service: HelperService
  ) {}

  ngOnInit() {
    this.getIncidents()
  }

  getIncidents() {
    this.service.getIncidents()
      .subscribe(
        (res:any) => {
          this.dataSource = res.data
        }
      )
  }

  deleteIncident(id:number) {
    this.service.deleteIncident(id)
      .subscribe(
        (res) => {
          this.getIncidents()
        }
      )
  }

}

