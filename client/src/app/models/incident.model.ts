export interface Incident {
  id: number
  title: string
  description: string
  criticality: string
  type: string
  status: string
}
