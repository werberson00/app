import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { FormBuilder, Validators } from '@angular/forms';
import { HelperService } from '../services/helper.service';
import { Incident } from '../models/incident.model';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  id:number
  incidents:any

  formIncident = this.fb.group({
    title: [''],
    description: [''],
    criticality: [''],
    type: [''],
    status: ['']
  })

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private _location: Location,
    private service: HelperService,
    private router: Router
  ) { }

  ngOnInit() {
    let id
    this.route.paramMap.subscribe(
      params => {
        id = params.get('id')
      }
    )
    this.getIncident(id)
  }

  getIncident(id) {
    this.service.getIncident(id)
      .subscribe(
        (res) => {
          this.incidents = res
          this.formIncident.controls['title'].setValue(this.incidents.data.title)
          this.formIncident.controls['criticality'].setValue(this.incidents.data.criticality)
          this.formIncident.controls['description'].setValue(this.incidents.data.description)
          this.formIncident.controls['type'].setValue(this.incidents.data.type)
          this.formIncident.controls['status'].setValue(this.incidents.data.status)
        }
      )
  }

  updateIncident(id) {
    let obj = this.formIncident.value
    this.service.updateIncident(obj, id)
      .subscribe(
        (res) => {
          this.router.navigate(['/'])
        }
      )
  }

  back() {
    this._location.back()
  }

}
