import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { FormBuilder, Validators } from '@angular/forms';
import { HelperService } from '../services/helper.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  formIncident = this.fb.group({
    title: [''],
    description: [''],
    criticality: [''],
    type: [''],
    status: ['']
  })

  constructor(
    private fb: FormBuilder,
    private _location: Location,
    private service: HelperService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  cadastra() {
    let obj = this.formIncident.value
    this.service.createIncident(obj)
      .subscribe(
        (res) => {
          this.router.navigate(['/'])
        }
      )
  }

  back() {
    this._location.back()
  }

}
