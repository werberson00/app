import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Incident } from '../models/incident.model';
import { HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HelperService {

  readonly url:string = 'http://localhost:8000/api/v1'

  constructor(private http: HttpClient) { }

  getIncidents():Observable<Incident[]> {
    return this.http.get<Incident[]>(`${this.url}/incidents`)
  }

  getIncident(id):Observable<Incident> {
    return this.http.get<Incident>(`${this.url}/incidents/${id}`)
  }

  deleteIncident(id):Observable<Incident> {
    return this.http.delete<Incident>(`${this.url}/incidents/${id}`)
  }

  createIncident(i: Incident) {
    return this.http.post(`${this.url}/incidents`, i)
  }

  updateIncident(i: Incident, id:number) {
    return this.http.put(`${this.url}/incidents/${id}`, i)
  }
}
