## Requerimentos
1. Angular-cli
2. Node
3. PHP >= 7.0
4. Mysql
5. Composer

## Como configurar e iniciar o sistema

1. Abra a pasta server em um editor de sua preferência edite o arquivo .env com o nome da base de dados que você irá criar e com seu usuário e senha do banco de dados
2. Em seguida crie um banco de dados com as mesmas configurações inseridas no arquivo .env
3. Abra um terminal e navegue até a pasta server e execute os seguintes comandos composer install, php artisan migrate e depois inicie o servidor com o comando php artisan serve.
4. Em seguida abra mais um terminal e navegue até a pasta client e execute o comando npm install e depois inicie o servidor com o comando ng serve
5. Depois é só abrir a url http://localhost:4200 em um navegador de sua preferência

---
